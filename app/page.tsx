"use client";
import "@/src/styles/resst.scss";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import PizzaListCard from "@/app/PizzaListCard";
import Header from "@/src/components/Header/Header";


type PizzaObject = {
    img: string;
    name: string;
    price: number;
    ingredients: Array<string>;
    id: string;
};

const HomePage = () => {

    const [pizzaList, setPizzaList] = useState<PizzaObject[]>([]);

    useEffect(() => {
        async function fetchData() {
            const response = await fetch("data.json");
            const data = await response.json();
            setPizzaList(data.pizza);
        }
        fetchData();
    }, []);

    return (
        <>
            <Header />
            <section className="container">
                <img className="home_img" src="/pizza_img.jpg" alt="pizza" />
               <h3 className="home_title">Піца</h3>
                <div className="container_pizza_card">
                    {pizzaList && pizzaList.map((pizzaItem) => (
                        <div className="card_item" key={pizzaItem.id}>
                     <PizzaListCard idProduct ={`/${pizzaItem.id}`} pizzaItem={pizzaItem} />
                        </div>
                    ))}
                </div>
            </section>
            <Link href="/half_pizza">Half Pizza</Link>
            <Link href="/your_pizza">Your Pizza</Link>
        </>
    );
};

export default HomePage;
