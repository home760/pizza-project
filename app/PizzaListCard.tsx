"use client"

import React, {FC} from "react"
import "./styles/PizzaListCard.scss"
import Button from "@/src/components/Button/Button"
import Link from "next/link"
import {productInfavorite} from "@/src/redux/features/favorite-slice"
import {useDispatch} from "react-redux"
import { AppDispatch } from "@/src/redux/store"
import { useAppselector } from "@/src/redux/store";
import FavoriteCard from "@/src/components/Header/svg-header/FavoriteCard"
type PizzaObject = {
    img: string,
    name: string,
    price: number,
    ingredients: Array<string>,
}
interface PizzaListCardProps {
    pizzaItem: PizzaObject,
    idProduct: string
}
const PizzaListCard: FC<PizzaListCardProps> = ({pizzaItem,idProduct}) => {
    const {img, name, price, ingredients} = pizzaItem
    const dispatch = useDispatch<AppDispatch>()
    const favorites = useAppselector((state)=> state.favoriteReducer.favorites)
    const isFavorite = favorites.includes(name);
    const getProduct = ()=>{
     dispatch(productInfavorite(name))
    }

    return (
        <>
             <div>
            <div className="card_imgs">
                <FavoriteCard onClick={()=> getProduct()} className={`favorite_card-color ${isFavorite ? 'active' : ''}`}/>
                <Link href={idProduct}>
                <img  className="img_card" src={img} alt={name} />  
                </Link> 
             </div>
               
            <div className="cart_content">
            <p className="card_name">{name}</p>
            <p className="card_price">{price} грн</p>
            <div  className="card_ingredient">
            {ingredients.map((ingredient, index) => (
                <span className="inredient_item" key={index}>
                    {ingredient}
                    {index < ingredients.length - 1 && ', '}
                    </span>
            ))}
            </div>
           <Button  className="card_button" green type="button" >Швидко</Button>
        </div>
            </div>

        
        </>
   
       
    )
}
export default PizzaListCard