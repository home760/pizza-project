import React, {FC} from "react";
import cn from "classnames";
import './styles/Button.scss'

interface ButtonProps {
    type?: "button" | "submit" | "reset";
    children: any;
    className?: string;
    green?: boolean;
    onClick?: () => void
}
const Button: FC<ButtonProps> = ({type="button", children, className, green, onClick}) => {
    return (
        <button 
        type={type} 
        className={cn('button', className, {'_green': green})}
        onClick={onClick}
        >
            {children}
        </button>
    )
}
export default Button