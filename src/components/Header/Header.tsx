import React from "react";
import './Header.scss'
import Search from "./svg-header/Search";
import Favorite from "./svg-header/Favorite";
import Basket from "./svg-header/Basket";
import DropDaun from "./DropDaun";
import { useAppselector } from "@/src/redux/store";
import Link from "next/link";
const Header = () =>{
    const favorites = useAppselector((state)=> state.favoriteReducer.favorites)
    return(
       <header className="header">
        <div className="container header_container">
        <div className="header_menu">
        <div className="menu_burger">
        <span className="burger_top"></span>
        <span className="burger_middle"></span>
        <span className="burger_bottom"></span>
        </div>
        <p className="menu_text">Список категорій</p>
        <DropDaun className="menu_DropDaun"/>
        </div>
        <div className="header_icons">
        <div className="header_search">
        <input className="search_input" type="text" placeholder="Знайдіть смачненьке тут" />
        <button className="search_button" type="button"><Search /></button>
        </div>
        <button className="icons_favorite" type="button"><Link href='/favorite'><Favorite/></Link></button>
        <div className="container_favorite_counter">
        <span className="favorite_counter">{favorites.length}</span>
        </div>
      <Link className="basket_counter" href="/basket">
        <button className="icons_basket" type="button"><Basket/></button>
        <span>0 грн</span>
      </Link>
        </div>
        </div>
       </header> 
    )
}

export default Header