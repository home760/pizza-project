import React , {FC}from "react";
import Link from "next/link";
interface DropDaunProps {
  className?: string; 
}
const DropDaun: FC<DropDaunProps> = ({className}) =>{
    const arr = ["Піца", "Половинки","Разом дешевше","Салати", "Десерти","Соуси","Напої"]
    const imgArr = ["https://lviv.veteranopizza.com/image/cache/webp/catalog/category_pizza-30x30.webp","https://lviv.veteranopizza.com/image/cache/webp/catalog/polovinki-30x30.webp","https://lviv.veteranopizza.com/image/cache/webp/catalog/razom-deshevshe-30x30.webp", "https://lviv.veteranopizza.com/image/cache/webp/catalog/category_salad-30x30.webp","https://lviv.veteranopizza.com/image/cache/webp/catalog/cheesecake-30x30.webp","  https://lviv.veteranopizza.com/image/cache/webp/catalog/category_soy-sauce-30x30.webp", "  https://lviv.veteranopizza.com/image/cache/webp/catalog/category_soy-sauce-30x30.webp", "https://lviv.veteranopizza.com/image/cache/webp/catalog/category_drink-30x30.webp"]
    const linkArr = ["pizza","polovinki","razom-deshevshe","salati","deserti","sousi","napoi"]
return(
<nav className={className}>
<ul className="dropDaun_nav">
{
    arr.map((element,index)=>(
        <Link href={`/${linkArr[index]}`} key={element}>
            <li className="nav_menu">
                    <img className="menu_img" src={imgArr[index]} alt={element} />
                    {element}      
            </li>
        </Link>
    ))
}
</ul>
</nav>
)
}

export default DropDaun