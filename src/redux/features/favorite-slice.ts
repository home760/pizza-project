import  {createSlice, PayloadAction} from "@reduxjs/toolkit"



type favoriteState ={
    favorites: string[];
}

const initialState={
        favorites: [],
    
} as favoriteState



export const favoriteSlice = createSlice({
    name:'favorite',
    initialState,
    reducers:{
        productInfavorite: (state,action:PayloadAction<string>) =>{
        const favorite = action.payload;
        const index = state.favorites.indexOf(favorite);
        if (index === -1) {
        state.favorites.push(favorite);
        } else {
         state.favorites.splice(index, 1);
            }
               
            
        },
       
    }
})

export const {productInfavorite} = favoriteSlice.actions
export default favoriteSlice.reducer 