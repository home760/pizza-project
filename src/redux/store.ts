import {configureStore} from '@reduxjs/toolkit'
import favoriteReducer from './features/favorite-slice'
import { TypedUseSelectorHook, useSelector } from 'react-redux'

export const store = configureStore({
    reducer: {
        favoriteReducer,
    }
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export const useAppselector: TypedUseSelectorHook<RootState> = useSelector